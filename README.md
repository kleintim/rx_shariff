Shariff Extension
=================

The rx_shariff extension provides easy integration of [Heise Shariff](https://github.com/heiseonline/shariff)
into TYPO3 CMS and includes the frontend scripts as well as the [PHP data backend](https://github.com/heiseonline/shariff-backend-php) to retrieve social media stats.

This TYPO3 extension is created and maintained by: https://reelworx.at/
